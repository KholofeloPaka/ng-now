import { NgNowPage } from './app.po';

describe('ng-now App', function() {
  let page: NgNowPage;

  beforeEach(() => {
    page = new NgNowPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
