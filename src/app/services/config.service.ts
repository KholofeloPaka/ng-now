import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import 'rxjs/add/operator/toPromise';

/**
 * A config service application that reads application meta data from config file.
 * @Module ConfigService
 * @author Paka Kholofelo
 * @since 03/08/2017
 */
@Injectable()
export class ConfigService {

    private configData: JSON;

    /**
     * Create an instance of config service
     * @param {Http} http   The htpp service being injected
     */
    public constructor(
        private http: Http) {
    }

    /**
     * Load data from a config json
     * @param {string} path   The path to load a config json from
     * @return {*}
     */
    public load(path = 'environments/config.json'): Promise<void> {
        return this.http.get(path)
            .toPromise()
            .then(config => {
                this.configData = config.json();
            }).catch(err => {

            });
    }

    /**
     * Get config data by key
     * @param {string} string   The key being used to retrive config data
     * @return mixed configData;
     */
    public get(key: string): any {
        return this.configData[key];
    }

}
