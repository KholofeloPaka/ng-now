import { Inject, Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ConfigService } from './config.service';

/**
 * This sevesrity levels are defined by syslog at https://tools.ietf.org/html/rfc5424
 */
enum SEVERITY_LEVEL {
    ERROR = 0,
    WARN = 4,
    INFO = 6,
    DEBUG = 7
}

/**
 * A logger service that provide the same functions as {@link console}.
 * The logger is binded to the web server logs.
 * @Module LoggerService
 * @author Paka Kholofelo
 */
@Injectable()
export class LoggerService {

    private logs: Array<any> = [];
    private interval: number;
    private lastError: any;

    /**
     * Create an instance of LoggerService.
     * @param {ConfigService} configService     The Config Service being injected.
     * @param {ApiService} apiService           The Api Service being injected.
     */
    constructor(
        @Inject(ApiService) private apiService: ApiService,
        @Inject(ConfigService) private configService: ConfigService) {

        this.interval = 3000;
        // setTimeout(this.sendToServer(), this.interval);
    }

    /**
     * Outputs an info message.
     * @param {*} message          The message being logged.
     * @return void
     */
    public info(...message): void {
        this.addLog(SEVERITY_LEVEL.INFO, message);
    }

    /**
     * Outputs an warning message.
     * @param {*} message          The message being logged.
     * @return void
     */
    public warn(...message): void {
        this.addLog(SEVERITY_LEVEL.WARN, message);
    }

    /**
     * Outputs an error message.
     * @param {*} message  The message being logged.
     * @return void
     */
    public error(...message): void {
        this.addLog(SEVERITY_LEVEL.ERROR, message);
    }

    /**
     * Outputs a debug message.
     * @param {*} message  The message being logged.
     * @return void
     */
    public debug(...message): void {
        this.addLog(SEVERITY_LEVEL.DEBUG, message);
    }

    /**
     * Add log messages to a log variable.
     * @param {*} message      The message being logged.
     * @param {number} severity     The severity level of the message being logged.
     * @return void
     */
    private addLog(severity: number, message): void {
        if (this.lastError !== message) {
            this.logs.push('Sererity Level:' + severity + '->' + message);
            this.lastError = message;
        }
    }

    /**
     * Send logs to a server.
     * @return void
     */
    private sendToServer(): void {
        if (this.logs.length > 0) {
            let logPath = this.configService.get('logs');
            if ( typeof logPath !== 'undefined' ) {
                // this.apiService.get(logPath, this.logs);
            }
            this.logs = [];
        }
    }
}
