import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Http, Response } from '@angular/http';

import { ConfigService } from './config.service';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/**
 * An api service that initiate a rest api call.
 * @Module ApiService
 * @author Paka Kholofelo
 * @since 03/08/2017
 */
@Injectable()
export class ApiService {

    private host: string;
    /**
     * Create an instance of ApiService
     *
     * @param {Http} http                     The http module being injected
     * @param {ConfigService} configService   The config Service being injected
     */
    constructor(
        private http: Http,
        private configService: ConfigService) {

        this.host = this.configService.get('host');
    }

    /**
     *  Get data from the api
     *
     * @param {string} method   The method that must be called in the api
     * @param {model} model     The data model being posted to the backend api
     *
     * @return {Observable<*>}  The observable data model return from the api
     */
    public get(method: string, body: any): Observable<any> {
        const headers = new Headers({ 'Content-Type': 'application/json',
                                    'Accept': 'application/json'});
        const options = new RequestOptions({ headers: headers });

        return this.http
                    .get(this.host + method)
                    .map((response: Response) => response.json())
                    .catch(error => this.handleApiError(error));
    }

    /**
     * Extract the api response call
     *
     * @param {Response} res  The response returned by server call
     */
    private extracApiResponse(res: Response) {
        const body = res.json();
        return body;
    }

    /**
     * Handling Api Errors
     *
     * @param {Response} error  The response error returned by server call
     */
    private handleApiError(error: Response | any) {
        return Observable.throw(error.message || error);
    }

    /**
     * Set host url
     * @param {string} host   The host url being set
     * @return void
     */
    private setHost(host: string): void {
        this.host = host;
    }

}
